<!DOCTYPE html>
<html lang="en">
<head>
  <title>Piscine</title>

  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="">

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700%7CPT+Serif:400,700,400italic%7COpen+Sans:400,600,700,400italic' rel='stylesheet'>

  <!-- Css -->
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/magnific-popup.css" />
  <link rel="stylesheet" href="css/font-icons.css" />
  <link rel="stylesheet" href="revolution/css/settings.css" />
  <link rel="stylesheet" href="css/rev-slider.css" />
  <link rel="stylesheet" href="css/sliders.css">
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/responsive.css" />
  <link rel="stylesheet" href="css/spacings.css" />
  <link rel="stylesheet" href="css/animate.css" />
  <link rel="stylesheet" href="css/color.css" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="img/favicon.ico">
  <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

</head>

<body style="font-family: 'Open Sans', sans-serif;">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <header class="nav-type-2" >
    
    <nav class="navbar navbar-static-top" style="background-color:#B6DFF8;">
      <div class="navigation">

        <div class="container relative">

          <form method="get" class="search-wrap">
            <input type="search" class="form-control" placeholder="Type &amp; Hit Enter">
          </form>

          <div class="row">

            <div class="navbar-header">
              <!-- Logo -->
              <div class="logo-container">
                <div class="logo-wrap">
                  <a href="index-mp.html">
                    <img class="logo" src="img/logo_dark.png" alt="logo">
                  </a>
                </div>
              </div>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div> <!-- end navbar-header -->

            <div class="col-md-9 nav-wrap right">
              <div class="collapse navbar-collapse" id="navbar-collapse">
                
                <ul class="nav navbar-nav navbar-right" >

                  <li class="menu">
                    <a href="#" style="color:white;">Accueil</a>
                  </li>

                  <li>
                    <a href="#qui-sommes-nous" style="color:white;">Qui sommes-nous?</a>
                  </li>

                  <li>
                    <a href="#realisations" style="color:white;" >Nos réalisations</a>
                  </li>

                  <li>
                    <a href="#contact" style="color:white;">Nous contacter</a>
                  </li>

                  <li>
                    <a href="#" style="color:white;">05 xx xx xx xx</a>
                  </li>
                  
                </ul> <!-- end menu -->
              </div> <!-- end collapse -->
            </div> <!-- end col -->
            
          </div> <!-- end row -->
        </div> <!-- end container -->
      </div> <!-- end navigation -->
    </nav> <!-- end navbar -->
  </header>


  <div class="main-wrapper-mp oh">

    <!-- Revolution Slider -->
    <section> 
      <div class="rev_slider_wrapper">
        <div class="rev_slider" id="slider2" data-version="5.0">
          <ul>
            <!-- SLIDE 1 -->
            <li 
              data-fstransition="fade"
              data-transition="cube"
              data-easein="default" 
                data-easeout="default"
              data-slotamount="default"
              data-saveperformance="off"
              data-masterspeed="1000"
              data-delay="8000"
              data-title="The Art of Design">
              <!-- MAIN IMAGE -->
              <img src="img/piscine1.jpg"
                alt=""
                data-bgrepeat="no-repeat"
                data-bgfit="cover"
                class="rev-slidebg"
                >

              <!-- LAYER NR. 1 -->
              <div class="tp-caption hero-text giant_nocaps"
                data-x="center"
                data-y="center"
                data-voffset="[0,0,0,-20]"
                data-fontsize="[116,100,70,50]"
                data-textAlign="center"
                data-lineheight="[116,100,70,50]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:1500;"
                data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.1;sY:0.1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                data-transform_out="opacity:0;sX:0;sY:0;s:1200;e:Power3.easeInOut;"
                data-start="1000"
                data-splitout="none">Modern Design
              </div>
              
              <!-- LAYER NR. 2 -->
              <div class="tp-caption subheading_text"
                data-x="center"
                data-y="center"
                data-voffset="84"
                data-fontsize="[22,22,22,24]"
                data-textAlign="center"
                data-lineheight="[22,22,22,32]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:1500;"
                data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                data-transform_out="opacity:0;sX:0;sY:0;s:1200;e:Power3.easeInOut;"
                data-start="1000">Super Clean With a Great Attention to Details
              </div>
        
            </li> <!-- end slide 1 -->

            <!-- SLIDE 2 -->
            <li data-transition="cube"
              data-slotamount="1"
              data-masterspeed="1000"
              data-delay="8000"
              data-title="Creative &amp; Emotional">
              <!-- MAIN IMAGE -->
              <img src="img/piscine2.jpg"
                alt=""
                data-bgrepeat="no-repeat"
                data-bgfit="cover"
                class="rev-slidebg"
                >

              <!-- LAYER NR. 1 -->
              <div class="tp-caption hero-text large_white"
                data-x="center"
                data-y="center"
                data-voffset="[-50,-50,-50,-50]"
                data-fontsize="[60,50,46,28]"
                data-textAlign="center"
                data-lineheight="[60,50,46,28]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:1500;"
                data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                data-transform_out="opacity:0;sX:0;sY:0;s:1200;e:Power3.easeInOut;"
                data-start="1000">Super Flexible Theme
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption hero-text large_white"
                data-x="center"
                data-y="center"
                data-voffset="[15,15,15,-15]"
                data-fontsize="[60,50,46,28]"
                data-textAlign="center"
                data-lineheight="[60,50,46,28]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:1500;"
                data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                data-transform_out="opacity:0;sX:0;sY:0;s:1200;e:Power3.easeInOut;"
                data-start="1000">Suits for any Business
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption tp-resizeme"
                data-x="center"
                data-y="center"
                data-hoffset="0"
                data-voffset="[105,105,105,60]" 
                data-transform_idle="o:1;s:1500;"
                data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                data-transform_out="opacity:0;sX:0;sY:0;s:1200;e:Power3.easeInOut;"
                data-start="1000"
                style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">
              </div>  
        
            </li> <!-- end slide 2 -->

            <!-- SLIDE 3 -->
            <li data-transition="cube"
              data-slotamount="1"
              data-masterspeed="1000"
              data-delay="8000"
              data-title="Amazing Agency">
              <!-- MAIN IMAGE -->
              <img src="img/piscine4.jpg"
                alt=""
                data-bgrepeat="no-repeat"
                data-bgfit="cover"
                class="rev-slidebg"
                >

              <!-- LAYER NR. 1 -->
              <div class="tp-caption hero-text medium"
                data-x="30"
                data-y="center"
                data-voffset="-90"
                data-fontsize="[46,42,38,28]"
                data-lineheight="[46,42,38,28]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:1000"
                data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:1000;"
                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                data-start="1000"
                style="padding:0;">Outstanding Quality
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption small_text"
                data-x="30"
                data-y="center"
                data-voffset="-30"
                data-fontsize="[16,16,16,18]"
                data-lineheight="[16,16,16,16]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:900"
                data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:1000;"
                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                data-start="1100">Super Clean and Minimal Design
              </div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption small_text"
                data-x="30"
                data-y="center"
                data-voffset="0"
                data-fontsize="[16,16,16,18]"
                data-lineheight="[16,16,16,16]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:800"
                data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:1000;"
                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                data-start="1200">Awesome Features with Endless Possibilities
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption small_text"
                data-x="30"
                data-y="center"
                data-voffset="30"
                data-fontsize="[16,16,16,18]"
                data-lineheight="[16,16,16,16]"
                data-width="[none, none, none, 400]"
                data-whitespace="[nowrap, nowrap, nowrap, normal]"
                data-transform_idle="o:1;s:700"
                data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:1000;"
                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                data-start="1300">Fully Responsive and Retina Ready
              </div>

              <!-- LAYER NR. 7 -->
              <div class="tp-caption tp-resizeme"
                data-x="30"
                data-y="center"
                data-voffset="100"
                data-hoffset="0" 
                data-transform_idle="o:1;s:600"
                data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:1000;"
                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                data-start="1400"
                style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption tp-resizeme"
                data-x="204"
                data-y="center"
                data-voffset="100"
                data-hoffset="0"
                data-transform_idle="o:1;s:600"
                data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:1000;"
                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                data-start="1400"
                style="z-index: 13; max-width: auto; max-height: auto; white-space: nowrap;">
              </div>
            
            </li> <!-- end slide 3 -->

          </ul>

        </div>
      </div>
    </section>


    <!-- Our Services -->
    <section id="qui-sommes-nous" class="section-wrap-mp services style-2 pb-40 pb-mdm-50">
      <div class="container">

        <div class="row">
          <div class="col-md-4 service-item">
            <a href="#">
              <i class="icon icon-DesktopMonitor"></i>
            </a>
            <div class="service-item-box">
              <h3>Great Design</h3>
              <p>Our web design team will spend time with our digital marketing team to ensure the core principles of effective websites.</p>
            </div>
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <a href="#">
              <i class="icon icon-Layers"></i>
            </a>
            <div class="service-item-box">
              <h3>Perfect Coding</h3>
              <p>Our web design team will spend time with our digital marketing team to ensure the core principles of effective websites.</p>
            </div>
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <a href="#">
              <i class="icon icon-Eye"></i>
            </a>
            <div class="service-item-box">
              <h3>Retina Ready</h3>
              <p>Our web design team will spend time with our digital marketing team to ensure the core principles of effective websites.</p>
            </div>
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <a href="#">
              <i class="icon icon-User"></i>
            </a>
            <div class="service-item-box">
              <h3>5 Star Support</h3>
              <p>Our web design team will spend time with our digital marketing team to ensure the core principles of effective websites.</p>
            </div>
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <a href="#">
              <i class="icon icon-Settings"></i>
            </a>
            <div class="service-item-box">
              <h3>Easy to Customize</h3>
              <p>Our web design team will spend time with our digital marketing team to ensure the core principles of effective websites.</p>
            </div>
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <a href="#">
              <i class="icon icon-Timer"></i>
            </a>
            <div class="service-item-box">
              <h3>Fast Loading</h3>
              <p>Our web design team will spend time with our digital marketing team to ensure the core principles of effective websites.</p>
            </div>
          </div> <!-- end service item -->
          
        </div>
      </div>
    </section> <!-- end services -->
    

    <!-- Portfolio -->
    <section id="realisations" class="section-wrap-mp pb-60" >
      <div class="container">

        <div class="row heading">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <h2 class="text-center bottom-line">Nos réalisations</h2>
            <p class="subheading text-center">We Take Care About Every Detail</p>
          </div>
        </div>

        <!-- filter -->
        <div class="row"> 
          <div class="col-md-12">
            <div class="portfolio-filter">
              <!-- <a href="#" class="filter active" data-filter="*">All</a> -->
              <a href="#" class="filter" data-filter=".interieur">Piscine intérieure</a>
              <a href="#" class="filter" data-filter=".public">Piscine publique</a>
              <a href="#" class="filter" data-filter=".forme">Forme libre</a>
              <a href="#" class="filter" data-filter=".debordement">Débordement</a>
              <a href="#" class="filter" data-filter=".renovations">Rénovations</a>
              <a href="#" class="filter" data-filter=".locaux">Locaux Techniques</a>
              
            </div>
          </div>
        </div> <!-- end filter -->

        <div class="row">

          <div class="works-grid titles">

            <div class="col-md-4 col-xs-6 work-item interieur" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/interieur1.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/interieur1.jpg" class="lightbox-gallery" title="Poster Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item interieur" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/interieur2.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/interieur2.jpg" class="lightbox-gallery"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item interieur" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/interieur3.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/interieur3.jpg" class="lightbox-gallery" title="Cup Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item public" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/public4.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/public4.jpg" class="lightbox-gallery"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item public" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/public2.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/public2.jpg" class="lightbox-gallery" title="Brod Identity"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item public" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/public3.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/public3.jpg" class="lightbox-gallery" title="Cup Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item forme" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/forme3.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/forme3.jpg" class="lightbox-gallery" title="Poster Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item forme" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/forme4.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/forme4.jpg" class="lightbox-gallery"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item forme" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/forme1.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/forme1.jpg" class="lightbox-gallery" title="Cup Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->


            <!-- <div class="works-grid titles"> -->

            <div class="col-md-4 col-xs-6 work-item renovations" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/renovation2.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/renovation2.jpg" class="lightbox-gallery"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item renovations" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/renovation3.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/renovation3.jpg" class="lightbox-gallery" title="Cup Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item renovations" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/renovation1.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/renovation1.jpg" class="lightbox-gallery" title="Poster Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item locaux" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/locaux1.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/locaux1.jpg" class="lightbox-gallery" title="Poster Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item locaux" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/locaux2.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/locaux2.jpg" class="lightbox-gallery"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item locaux" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/locaux3.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/locaux3.jpg" class="lightbox-gallery" title="Cup Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->
            
            <div class="col-md-4 col-xs-6 work-item debordement" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/debordement.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/debordement.jpg" class="lightbox-gallery"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item debordement" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/debordement4.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/debordement4.jpg" class="lightbox-gallery" title="Brod Identity"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->

            <div class="col-md-4 col-xs-6 work-item debordement" onclick="void(0)">
              <div class="work-container">
                <div class="work-img">
                  <img src="img/debordement2.jpg" alt="">
                  <div class="portfolio-overlay">
                    <div class="project-icons">
                      <a href="img/debordement2.jpg" class="lightbox-gallery" title="Cup Mockup"><i class="fa fa-search"></i></a>
                    </div>
                  </div>
                </div>
              </div> 
            </div> <!-- end work-item -->


            <!-- <div class="works-grid titles"> -->





          </div>

        </div>    
      </div>
    </section> <!-- end portfolio-->



    <!-- Contact -->
    <section class="section-wrap contact" id="contact">
      <div class="container">

        <div class="row heading">
          <div class="col-md-6 col-md-offset-3 text-center">
            <h2 class="text-center bottom-line">Nous contacter</h2>
            <p class="subheading">Dont hesitate to get touch please fill out this form and we'll get back to you within 48hrs. We hand picked to provide the right balance of skills to work.</p>
          </div>
        </div>

        <div class="row">

          <div class="col-md-4">
            <h5>Get in Touch</h5>
            <p>Mon-Fri: 8:00 – 20:00</p>

            <div class="contact-item">
              <div class="contact-icon">
                <i class="icon icon-Pointer"></i>
              </div>
              <h6>Address</h6>
              <p>Philippines,<br>
              Greenwich st. 256/6, 62058</p>
            </div> <!-- end address -->

            <div class="contact-item">
              <div class="contact-icon">
                <i class="icon icon-Phone"></i>
              </div>
              <h6>Call Us</h6>
              <span>+1 888 5146 3269</span>
            </div> <!-- end phone number -->

            <div class="contact-item">
              <div class="contact-icon">
                <i class="icon icon-Mail"></i>
              </div>
              <h6>E-mail</h6>
              <a href="mailto:enigmasupport@gmail.com">enigmasupport@gmail.com</a>
            </div> <!-- end email -->

          </div>

          <div class="col-md-8">
            <form id="contact-form" action="#">

              <div class="row contact-row">
                <div class="col-md-6 contact-name">
                  <input name="name" id="name" type="text" placeholder="Nom*">
                </div>
                <div class="col-md-6 contact-email">
                  <input name="mail" id="mail" type="email" placeholder="E-mail*">
                </div>
              </div>

              <input name="subject" id="subject" type="text" placeholder="Objet"> 
              <textarea name="comment" id="comment" placeholder="Message"></textarea>
              <input type="submit" class="btn btn-lg btn-color btn-submit" value="Envoyer" id="submit-message">
              <div id="msg" class="message"></div>
            </form>
          </div> <!-- end col -->

        </div>
      </div>
    </section> <!-- end contact -->

    <div id="back-to-top">
      <a href="#top"><i class="fa fa-angle-up"></i></a>
    </div>

  </div> <!-- end main-wrapper -->
  
  <!-- jQuery Scripts -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/plugins.js"></script>
  <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/rev-slider.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>


    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>

</body>
</html>